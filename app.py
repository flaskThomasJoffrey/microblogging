from functools import wraps
from var_dump import var_dump
from flask import Flask, render_template, redirect,flash,request, url_for,abort
import click
import requests
import datetime
from random import sample ,random

from tools.security import check_password, hash_Pasword
from form import UserForm, PostForm, LoginForm
from models import create_tables, drop_tables, User, Post
import os
SECRET_KEY = os.urandom(32)
from flask_login import login_user,logout_user, LoginManager, current_user
from peewee import DoesNotExist, IntegrityError
from playhouse.flask_utils import PaginatedQuery
# from flask_utils import PaginatedQuery

app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['DEBUG'] = True
login_manager = LoginManager()
login_manager.init_app(app)


#########
## Routes
#########

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
    
        if not current_user.is_authenticated:
            return redirect('/login', code=302)
        return f(*args, **kwargs)
    return decorated_function


# #Accueil
# @app.route('/')
# def index():
#     return render_template('home.html')

##
## POST
##

# Affichage d'un post
# @app.route('/posts/<int:page>')
@app.route('/posts/<int:idpost>/view')
@login_required
def post(idpost = None):
    pagination = None
    if idpost != None :
        try:
            post = Post.get(id=idpost)
            return render_template('post.html', post = post)

        except DoesNotExist:
            return redirect('/posts', code=302 )
# Accueil &  liste des posts
@app.route('/')
@app.route('/posts')
# @login_required
def posts_list():
    pagination = None
    pagination = PaginatedQuery(Post.select().order_by(Post.created_at.desc()),10, 'page')
 
    return render_template('posts_list.html', pagination = pagination,current_user = current_user)

# Affichage et édition d'un poste
@app.route('/posts/new',methods=['GET', 'POST', ])
@app.route('/posts/<int:idpost>/edit',methods=['GET', 'POST', ]) # , 
@login_required
def posts_edit(idpost = None):
    post = Post()
    user = User()
    try:
        
        user = User.get(id=current_user.get_id())
        if idpost != None :
            post = Post.get(Post.id==idpost, Post.user == user)

    except DoesNotExist:
        return abort(403)
    # print(user)
   
    form = PostForm(obj=post)
    if form.validate_on_submit():
        form.populate_obj(post)
        # post.created_at = datetime.datetime.now()
        post.updated_at = datetime.datetime.now()
        post.user = user
        try:
            post.save()
        except IntegrityError :
            form.title.errors.append("Le titre de l'article existe déjà") 
            return render_template('posts_edit.html', form=form,  id =idpost) 

        return redirect('/posts/' + str(post.id) + '/view' )
    # form.populate_obj(users)
    # form.model.choices = [(m.id, m.version) for m in IngenModel.select()]
    return render_template('posts_edit.html', form=form,  id =idpost)    

@app.route('/posts/<int:idpost>/delete',methods=['GET']) # ,
def delete_post(idpost):
    Post.delete().where(Post.id == idpost).execute()
    return redirect('/posts' )



##
## UTILISATEURS
##


# nouveau utilisateur 
@app.route('/register',methods=['GET', 'POST', ])
def register():
    
    user = User()
    # print(user)
    form = UserForm(obj=user)
    if form.validate_on_submit():
        form.populate_obj(user)
        user.password = hash_Pasword(user.password)
        try :
            user.save()
        except IntegrityError :
            form.user_name.errors.append("Utilisateur déjà utilisé") 
            return render_template('users_edit.html', form=form)
            


        if not current_user.is_authenticated:
            flash("Inscription réussie, vous pouvez vous connecter")
            return redirect('/login')
        else : 
            return redirect('/users/' + str(user.id) )
    # form.populate_obj(users)
    # form.model.choices = [(m.id, m.version) for m in IngenModel.select()]
    return render_template('users_edit.html', form=form)

#édition d'un utilisateur
# @app.route('/users/new',methods=['GET', 'POST', ])
@app.route('/users/<int:iduser>/edit',methods=['GET', 'POST', ]) # ,
@login_required 
def users_Edit(iduser ):
    if  iduser != current_user.id: # c'est un édit. Sino c'est un nouvel utilisateur
        abort(403)

    user = User()
    try:
        user = User.get(id=iduser)
        
    except DoesNotExist:
        abort(404)
    # print(user)
   
    form = UserForm(obj=user)
    

    
    if form.validate_on_submit():
        form.populate_obj(user)
        user.password = hash_Pasword(user.password)
        user.save()
        return redirect('/users/' + str(user.id) )
    # form.populate_obj(users)
    # form.model.choices = [(m.id, m.version) for m in IngenModel.select()]
    return render_template('users_edit.html', form=form, id =iduser )

# liste des posts d'un utilisateur
@app.route('/users/<int:idUser>/posts')
def users_posts(idUser):
    pagination = None
    post = Post()
    user = User()
    try:
        user = User.get(id=idUser)
        pagination = PaginatedQuery(Post.select().where(Post.user == user),10, 'page')
    except DoesNotExist:
        abort(404)
    return render_template('posts_list.html', pagination = pagination)

# liste des utilisateurs
@app.route('/users')
@login_required
def users():
    users = User.select()
    pagination = None
    pagination = PaginatedQuery(users.select(),10, 'page')
    return render_template('users_list.html', pagination = pagination,current_user = current_user)


# liste d'un utilisateur
@app.route('/users/<int:iduser>/view')
@login_required
def user(iduser = None):
    pagination = None
    if iduser != None :
        try:
            user = User.get(id=iduser)
            return render_template('user.html', user = user)

        except DoesNotExist:
            return redirect('/users', code=302 )

@app.route('/users/<int:iduser>/delete',methods=['GET']) # ,
@login_required

def delete_user(iduser):
    logout_user()
    if iduser == current_user.id :
        User.delete().where(User.id == iduser).execute()
        flash("Votre compte a été supprimé.")
    return redirect('/' )
    

##
## SESSION
##
@app.route('/login', methods=['GET', 'POST'])
def login():
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        user_name = form.user_name.data

        try:
            user = User.get(user_name=user_name)
        except DoesNotExist:
            form.user_name.errors.append("Utilisateur incorrect") 
            #  return render_template('login.html', form=form)
        else:

            if(check_password(user.password,form.password.data ) == False):
                
                form.password.errors.append("Mot de passe incorrect") 
            else :
                # flash('Mot de passe incorrect.')
                # return render_template('login.html', form=form)
            
                login_user(user)
                
                flash('Logged in successfully.') 

                next = request.args.get('next')
                # is_safe_url should check if the url is safe for redirects.
                # See http://flask.pocoo.org/snippets/62/ for an example.
                # if not is_safe_url(next):
                #     return abort(400)

                return redirect(next or url_for('posts_list'))
    return render_template('login.html', form=form)

@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect("/")
    

# 404 not found
@app.errorhandler(404)
def notFound(e):
    users = User.select()
    return render_template('404.html'),404
# 403 Forbidden
@app.errorhandler(403)
def notFound(e):
    users = User.select()
    return render_template('403.html'),403



@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)
################
# Commandes CLI
################
@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')

@app.cli.command()
def fakedata():
    from faker import Faker
    fake = Faker()
    for pk in range(0, 5):
        User.create(
            user_name=str(pk) + fake.user_name(), 
            first_name=fake.first_name(), 
            last_name=fake.last_name(), 
            email=fake.email(), 
            password=hash_Pasword("password")
        )
    User.create(
        user_name="toto", 
        first_name="toto", 
        last_name="toto", 
        email="toto", 
        password=hash_Pasword("toto")
    )
    for pk in range(0, 100):
        Post.create(
            user = random.randint(1,5),
            title = fake.text(),
            body = fake.text()
            )
    click.echo('Données créées')


# @app.cli.command()
# def resetdb():
#     dropdb()
#     initdb()
#     fakedata()        

 
 
 