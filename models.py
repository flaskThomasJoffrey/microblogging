
import datetime
from flask_login import UserMixin
from peewee import (
    SqliteDatabase, 
    Model,
    CharField,
    DateField,
    DateTimeField,
    FloatField,
    ForeignKeyField,
)

from peewee import *

database = SqliteDatabase("data.sqlite3")
class BaseModel(Model):

    class Meta:
        database = database
    
class User(BaseModel,UserMixin):
    user_name = CharField(unique=True)
    first_name = CharField()
    last_name = CharField()
    email = CharField()
    password = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)

   

    # def get_id() : 
    #     return self.id

class Post(BaseModel):
    user = ForeignKeyField(User, backref='poster')
    title = CharField(unique=True)
    body = TextField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)



    # GESTION TABLES
def create_tables():
    with database:
        database.create_tables([User,Post, ]) 


def drop_tables():
    with database:
        database.drop_tables([User,Post, ])