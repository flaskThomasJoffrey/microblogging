
import hashlib

def hash_Pasword(password):
    passwordHashed = hashlib.sha224(str(password).encode()).hexdigest()
    return passwordHashed

def check_password(passwordHashed, passwordToCheck):
    passwordToCheckHashed = hash_Pasword(passwordToCheck)
    return passwordHashed == passwordToCheckHashed