# lancer les test : 
# pytest tests/ --disable-warnings

import pytest
from models import  User, Post
from tools.security import hash_Pasword, check_password


def test_database_select():
    result = User.select()
    assert result  is not None # n est pas null
    assert isinstance(result, object) # est un tableau
    
    result = Post.select()
    assert result  is not None # n est pas null
    assert isinstance(result, object) # est un tableau


def test_security_function():
    password = "TOTO"
    passwordHashed = hash_Pasword(password)

    assert check_password(passwordHashed,"toto")  == False # mot de passe incorrect
    assert check_password(passwordHashed,password)  == True # mot de passe correct


# from flask_login import current_user
# from my_app.tests.base import BaseTest

# def test_a():
#     with self.app:
#         print(current_user)
    
