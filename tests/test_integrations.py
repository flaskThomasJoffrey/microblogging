# lancer les test : 
# pytest tests/ --disable-warnings

import os
import pytest
from app import app
from flask import url_for


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    # app.config['SECRET_KEY'] = "key"
    client = app.test_client()
    with app.app_context():
        pass
    yield client


def test_public_routes(client):
    # index
    rv = client.get('/')
    assert rv.status_code == 200
    
    #login
    rv = client.get('/login')
    assert rv.status_code == 200
    
    #inscription
    rv = client.get('/register')
    assert rv.status_code == 200

    #liste des posts
    assert client.get('/posts').status_code  == 200
    
    #liste des posts d'un tuilisateur
    assert client.get('/users/1/posts').status_code  == 200

    # routes innacessible sans être connecté : 
    assert client.get('/posts/new').status_code in  [302, 404] 
    assert client.get('/posts/1/view').status_code in  [302, 404] 
    assert client.get('/posts/1/edit').status_code in  [302, 404]  
    assert client.get('/posts/1/del').status_code in  [302, 404]  

    assert client.get('/users').status_code in  [302, 404]  
    assert client.get('/users/new').status_code in  [302, 404]  
    assert client.get('/users/1/view').status_code in  [302, 404]  
    assert client.get('/users/1/edit').status_code in  [302, 404] 
    assert client.get('/users/1/del').status_code in  [302, 404] 


    assert client.get('/logout').status_code in  [302, 404] 



def test_register(client) :
    #en post user_name=&first_name=&last_name=&email=&password=&created_at=2019-06-07&csrf_token= 
    rv = client.post('/register', data=dict(
        user_name = 'test_py',
        first_name = 'test_py',
        last_name = 'test_py',
        email = 'test_py',
        password = 'test_py',
        created_at = '2019-06-07',
        csrf_token = 'key'))
    # assert rv.data == 302 # login ok
    assert rv.status_code == 302 or rv.status_code == 200 # inscription ok ou utilisateur existe déjà



def client_login(client):
    rv = client.post('/login', data=dict(
        user_name = 'test_py', 
        password= 'test_py', 
        csrf_token = 'key'))
    # assert rv.data == 302 # login ok
    assert rv.status_code == 302 # login ok
    return client


def test_display_post_users(client):
    with client_login(client) as client:

        # /users liste des utilisateurs 
        rv = client.get('/users')
        assert rv.status_code == 200   
        

        # /posts liste des postes
        rv = client.get('/posts')
        assert rv.status_code == 200   


def test_new_post_user(client): 
    with client_login(client) as client:

        # formulaire pour nouveau poste
        rv = client.get('/posts/new')
        assert rv.status_code == 200

        #title=&body=&csrf_token=
        rv = client.post('/posts/new', data=dict(
        title = 'test_titre', 
        body= 'test_post', 
        csrf_token = 'key'))
       
        assert rv.status_code == 302 or rv.status_code == 200 # poste créé ou existe déjà 

def test_logout(client):
    with client_login(client) as client:
        
        # deconnexion 
        rv = client.get('/logout')
        assert rv.status_code == 302

        #on tente de réacceder aux postes
        assert client.get('/users').status_code in  [302, 404] 

# def test_users_code_200(client):
#     with client_login(client) as client:


        
#         # /signin inscription
#         rv = client.get('/users/new')
#         assert rv.status_code == 200


# def test_posts_code_200(client):
#     with client_login(client) as client:
#         # /posts -> liste des postes
#         rv = client.get('/posts')
#         assert rv.status_code == 200
        
#         # /posts/new/ -> création des postes / modif 
#         rv = client.get('/posts/new')
#         assert rv.status_code == 200
        
#         # /posts/{id} -> un poste
#         rv = client.get('/posts/1')
#         assert rv.status_code == 200
        
#         #  /user/{id}/posts/{page}  -> liste des postes d'un utilisateur avec pagegination
#         rv = client.get('/user/1/posts/1')
#         assert rv.status_code == 200

    
def test_posts_code_404(client):  
   
    rv = client.get('/posts/-54')
    assert rv.status_code == 404
    
    
    

# def test_add(client):
    
#         client.get('/')