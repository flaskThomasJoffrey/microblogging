from flask_wtf import FlaskForm
from wtforms import StringField, SelectField,  PasswordField, TextAreaField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length


class UserForm(FlaskForm):

    user_name = StringField("Nom d'utilisateur", validators=[
                       DataRequired(), Length(min=3, max=20)])
    first_name = StringField('Nom', validators=[
                       DataRequired(), Length(min=3, max=20)])
    last_name = StringField('Prénom', validators=[
                       DataRequired(), Length(min=3, max=20)])
    email = StringField('Mail')
    password = PasswordField('Mot de passe', validators=[
                       DataRequired(), Length(min=3, max=20)])
                  
    created_at = DateField("created_at")

class PostForm(FlaskForm):

    title = StringField("Titre", validators=[
                       DataRequired(), Length(min=3, max=50)])
    body =  TextAreaField('Contenu', validators=[
                       DataRequired(), Length(min=3, max=300)])
    

    # created_at = DateField("created_at")
    
    # model = SelectField('User')
    # 
    
    
class LoginForm(FlaskForm):
    user_name = StringField("Nom d'utilisateur", validators=[
                        DataRequired(), Length(min=3, max=20)])
    password = PasswordField('Mot de passe', validators=[
                       DataRequired(), Length(min=3, max=20)])