# Microblogging
*
## Introduction
Un microblog fait avec Flask, Bootstrap et SQLite.
Projet réalisé par Thomas CHATELLIER & Joffrey LINE, LPRGI Web 2018/2019

Il est basé sur le cahier des charges suivant:
https://www.delahayeyourself.info/modules/LP%20Web%20Dynamique/flask/projet/

##### On peut s'inscrire, se connecter et ajouter, lire, modifier, supprimer un post !

## Informations 

- Des données "test" se trouvent dans le fichier base de données data.sqlite3 
- Les testsUnitaires regroupés dans tests/
- Le dossier tools comprend un module security contenant les fonctions de hashage.